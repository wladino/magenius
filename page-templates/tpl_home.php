<?php
/**
 * Template Name: Front Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<!-- About Section Start -->
<div id="about_section">
    <div class="container">
        <div class="arrow-bar"><span><img src="<?php echo THEME_URI; ?>images/down-arrow.png" alt=""></span></div>
        <div class="col-lg-8 float-none center-block">
            <div class="about_content">
            	<?php
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
				?>
            </div>
    	</div>
    </div>
  </div>
<!-- About Section End -->
  
<!-- Bio Section Start -->
<div id="bio_section">
<div class="container">
    <div class="col-lg-10 float-none center-block">
        <div class="bio_content">
            <?php 
			$b1_title = get_post_meta(get_the_ID(),'wpcf-first-bio-title',true);
			$b1_content = get_post_meta(get_the_ID(),'wpcf-first-bio-content',true);
			
			echo ($b1_title)?'<h3>'.$b1_title.': </h3>':'';
			echo $b1_content;
			?>
  
            <div class="sep"></div>
            
            <?php 
			$b2_title = get_post_meta(get_the_ID(),'wpcf-second-bio-title',true);
			$b2_content = get_post_meta(get_the_ID(),'wpcf-second-bio-content',true);
			
			echo ($b2_title)?'<h3>'.$b2_title.': </h3>':'';
			echo $b2_content;
			?>            
        </div>
    </div>
</div>
</div>
<!-- Bio Section End -->

<?php
$args_work = array('post_type' => 'works','posts_per_page'=>-1);
// The Query
$the_query_work = new WP_Query( $args_work );

// The Loop
if ( $the_query_work->have_posts() ) 
{
	?>
<!-- Slider Section Start -->
<div id="slider_section">
	<div class="work_slider">
    <?php
	$count = 1;
	while ( $the_query_work->have_posts() ) 
	{
		$the_query_work->the_post();
		?>
        <!-- Slider Item Start -->
        <div class="item">
            <div class="content_box">
                <a href="#" data-toggle="modal" data-target="#item_popup<?php echo $count?>">
                    <h3><?php the_title();?></h3>
                    <?php $works = get_the_terms(get_the_ID(),'work-categories');
					if($works != null)
					{
						foreach($works as $work)
						{
							echo '<span>('.$work->name.')</span> ';
						}
					}
					?>
                </a>
            </div>
        </div>
        <!-- Slider Item End -->
        <?php
		$popup .= '
		<div class="modal fade" id="item_popup'.$count.'" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="content_block">
							<h3>'.get_the_title().'</h3>
							<div class="sep">------------</div>
							<p>'.get_the_content().'</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		';
	$count++;
	}
	?>
    </div>
    <?php echo $popup;?>

</div>
<!-- Slider Section End -->
    <?php
} 
else 
{
	echo "<p>Sorry, No works added yat</p>";
}
/* Restore original Post Data */
wp_reset_postdata();
?>
<!-- Contact Section Start -->
<div id="contact_section">
<div class="container">
    <div class="col-lg-10 float-none center-block">
        <div class="contact_content">
            <?php 
			$section_heading = get_post_meta(get_the_ID(),'wpcf-section-title',true);
			echo ($section_heading)?'<h3>'.$section_heading.':</h3>':'' ?>
            
            <div class="clearfix">
                <div class="left-content">
                	 <?php 
					$section_content = get_post_meta(get_the_ID(),'wpcf-section-content',true);
					echo ($section_content)?'<p>'.$section_content.'</p>':'';
					
					$femail = get_post_meta(get_the_ID(),'wpcf-first-email-address',true);
					$fcontact = get_post_meta(get_the_ID(),'wpcf-first-contact-number',true);
					$final_data = '<a href="mailto:'.$femail.'">'.$femail.'</a><br> '.$fcontact;
					
					$semail = get_post_meta(get_the_ID(),'wpcf-second-email-address',true);
					$scontact = get_post_meta(get_the_ID(),'wpcf-second-contact-number',true);
					$second_data = '<a href="mailto:'.$semail.'">'.$semail.'</a><br> '.$scontact;
					
					echo ($final_data)?'<p>'.$final_data.'</p>':'';
					echo ($second_data)?'<p>'.$second_data.'</p>':'';
					?>
                </div>
                <div class="right-content">
                    <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Contact Section End -->
<?php
get_footer();