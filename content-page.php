<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="col-lg-8 float-none center-block">
    <div class="about_content">
        <h3 style="margin-top:20px;"><?php the_title(); ?></h3>
        <?php
            the_content();
        ?>
    </div>
</div>
