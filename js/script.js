/*========== Owl Carousel start ================*/
jQuery(document).ready( function() 
{
	jQuery('.work_slider').owlCarousel({
		autoplay:false,
		nav:true,
		navText: false,
		dots:false,
		smartSpeed: 1500,
		margin:27,
		responsiveClass:true,
		responsive:{
			0:{
				items:1
			},
			570:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			},
			1200:{
				items:4
			}
		}
	});
	
	var topMenu = jQuery("#header .navbar-default .navbar-nav > li"),
    topMenuHeight = topMenu.outerHeight()+20,
    // All list items
    menuItems = topMenu.find("a"),
	//alert(menuItems);
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function()
	{
    	var item = jQuery(jQuery(this).attr("href"));
      	if (item.length) { return item; }
    });

	// Bind to scroll
	jQuery(window).scroll(function()
	{
		// Get container scroll position
	   var fromTop = jQuery(this).scrollTop()+topMenuHeight+20;
	
	   // Get id of current scroll item
	   var cur = scrollItems.map(function(){
		 if (jQuery(this).offset().top < fromTop)
		   return this;
	   });
	   // Get the id of the current element
	   cur = cur[cur.length-1];
	   var id = cur && cur.length ? cur[0].id : "";
	  //alert(id);
	   // Set/remove active class
	   menuItems.parent().removeClass("active").end().filter("[href=#"+id+"]").parent().addClass("active");
	});
	
	/*========== Sticky Header start ================*/

	jQuery(function () 
	{
		jQuery('#header').stickyNavbar({
		activeClass: "active",          // Class to be added to highlight nav elements
		sectionSelector: "scrollto",    // Class of the section that is interconnected with nav links
		animDuration: 250,              // Duration of jQuery animation
		startAt: 82,                     // Stick the menu at XXXpx from the top of the this() (nav container)
		easing: "linear",               // Easing type if jqueryEffects = true, use jQuery Easing plugin to extend easing types - gsgd.co.uk/sandbox/jquery/easing
		animateCSS: true,               // AnimateCSS effect on/off
		animateCSSRepeat: false,        // Repeat animation everytime user scrolls
		cssAnimation: "fadeInDown",     // AnimateCSS class that will be added to selector
		jqueryEffects: false,           // jQuery animation on/off
		jqueryAnim: "slideDown",        // jQuery animation type: fadeIn, show or slideDown
		selector: "a",                  // Selector to which activeClass will be added, either "a" or "li"
		mobile: false,                  // If false nav will not stick under 480px width of window
		mobileWidth: 480,               // The viewport width (without scrollbar) under which stickyNavbar will not be applied (due usability on mobile devices)
		zindex: 9999,                   // The zindex value to apply to the element: default 9999, other option is "auto"
		stickyModeClass: "sticky",      // Class that will be applied to 'this' in sticky mode
		unstickyModeClass: "unsticky"   // Class that will be applied to 'this' in non-sticky mode
	  });
	});
	
});
/*========== Owl Carousel end ================*/