<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

 	<!-- Footer Start -->
	<div id="footer">
  	<div class="container">
    	<p>&copy; <?php bloginfo('name')?> <?php date('Y')?> 
        	<?php 
			$footer_page = ot_get_option('footer_page');
			if($footer_page)
			{
			?>
            <a href="<?php echo get_the_permalink($footer_page);?>"><?php echo get_the_title($footer_page);?></a>
        	<?php
            }
			?>
        </p>
    </div>
  </div>
  	<!-- Footer End -->
<?php wp_footer(); ?>

</body>
</html>
