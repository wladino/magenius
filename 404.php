<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
 <!-- About Section Start -->
  <div id="about_section">
    <div class="container">
        <div class="arrow-bar"><span><img src="<?php echo THEME_URI ?>images/down-arrow.png" alt=""></span></div>
        <div class="col-lg-8 float-none center-block">
            <div class="about_content">
                <h3><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyfifteen' ); ?>:</h3>
                <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfifteen' ); ?></p>
           		<?php //get_search_form(); ?>
            </div>
    	</div>
    </div>
  </div>
  <!-- About Section End -->
<?php get_footer(); ?>