<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php /*?><title><?php wp_title( '|', true, 'right' ); ?></title><?php */?>
	<?php
	if (ot_get_option('favicon'))
	{
		$favicon_url = ot_get_option('favicon');
	}
	else
	{
		$favicon_url = get_template_directory_uri().'/images/favicon.png';
	}
	echo '<link rel="shortcut icon" href="'.$favicon_url.'" />';
	?>
    <!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- window code start  -->
    	<script>
        	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			  var msViewportStyle = document.createElement('style')
			  msViewportStyle.appendChild(
				document.createTextNode(
				  '@-ms-viewport{width:auto!important}'
				)
			  )
			  document.querySelector('head').appendChild(msViewportStyle)
			}
	  </script>
    <!-- window code end  -->
    
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  	<!-- Top Banner Start -->
	<div id="main_banner">
		<div id="header">
        	<div class="container">
        	<!-- Navigation Start -->
        	<nav class="navbar navbar-default">
            	<div class="navbar-header">
                	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                
                <?php
				$defaults = array(
					'menu'            => (is_front_page())?'top_menu':'Inner_menus',
					'container'       => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id'    => 'navbar',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'items_wrap'      => '<ul id="%1$s" class="nav navbar-nav">%3$s</ul>',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $defaults );
				?>
            </nav>
            <!-- Navigation End -->
            <?php 
            // social accounts
			$fb = ot_get_option('facebook');
			$tw	= ot_get_option('twitter');
			$in = ot_get_option('linkedin');
			$ins = ot_get_option('instagram');  
			if($fb || $tw || $in || $ins)
			{
			?>
            <!-- Social Links Start -->
            <div class="social-icons">
            	<ul>
				<?php
                echo ($tw)?' <li><a target="_blank" href="'.$tw.'"><img src="'.THEME_URI.'images/twitt-icon.png" alt="Twitter"></a></li>':'';		
				echo ($fb)?' <li><a target="_blank" href="'.$fb.'"><img src="'.THEME_URI.'images/fb-icon.png" alt="Facebook"></a></li>':'';		
				echo ($in)?' <li><a target="_blank" href="'.$in.'"><img src="'.THEME_URI.'images/in-icon.png" alt="linked In"></a></li>':'';		
				echo ($ins)?' <li><a target="_blank" href="'.$ins.'"><img src="'.THEME_URI.'images/insta-icon.png" alt="instagram"></a></li>':'';	
                ?>
                </ul>
            </div>
            <!-- Social Links End -->
            <?php
			}
			?>
        </div>
			
        </div>
        <?php
		$logo =  ot_get_option('logo');
		$dlogo = THEME_URI.'images/logo.png';
		?>
		<div class="logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php echo ($logo)?$logo:$dlogo; ?>" alt="Logo">
			</a>
		</div>
  	</div>
  	<!-- Top Banner End -->